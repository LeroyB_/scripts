import sys
import json

contacts_input = None
with open(sys.argv[1], "r", encoding="utf8") as JSON:
    contacts_input = json.load(JSON)
for entry in contacts_input["contacts"]:
    mobile_number = entry["Mobile Phone"]
    mobile_number = mobile_number.replace("+", "")
    mobile_number = mobile_number.replace(" ", "")
    if mobile_number.startswith("417"):
        with open(sys.argv[2], "r", encoding="utf8") as db_leak:
            if mobile_number in db_leak.read():
                print(entry["First Name"] + " " + entry["Last Name"])
